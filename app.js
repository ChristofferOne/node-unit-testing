const express = require('express')
const app = express()
const apiRoutes = require('./api/routes')
const httpRoutes = require('./http/routes')

// Define app middleware
app.use(express.json())
app.use(express.urlencoded({
  extended: true
}))

// Define request routes
app.use(apiRoutes)
app.use(httpRoutes)

// Launch app to listen to post
app.listen(8080, function () {
  console.log('App running on port 8080!')
})
