const { CalculateRent } = require('../../calculateRent')
const { describe, it } = require('mocha')
const { expect } = require('chai')

describe('Rent calculation', function () {
  it('getRoomCost() function age test', function (done) {
    const rooms = 1
    const rating = 3

    expect(CalculateRent(15, rooms, rating)).to.equal(2860)
    expect(CalculateRent(28, rooms, rating)).to.equal(2640)
    expect(CalculateRent(70, rooms, rating)).to.equal(1980)
    expect(CalculateRent(50, rooms, rating)).to.equal(2310)

    done()
  })

  it('Number of rooms test', function (done) {
    const age = 30
    const rating = 3

    expect(CalculateRent(age, 0, rating)).to.equal(0)
    expect(CalculateRent(age, 1, rating)).to.equal(2640)
    expect(CalculateRent(age, 2, rating)).to.equal(5280)
    expect(CalculateRent(age, 3, rating)).to.equal(7920)
    expect(CalculateRent(age, 5, rating)).to.equal(13200)
    expect(CalculateRent(age, 25, rating)).to.equal(66000)

    done()
  })

  it('Rating test', function (done) {
    const age = 30
    const rooms = 2

    expect(CalculateRent(age, rooms, 1)).to.equal(3360)
    expect(CalculateRent(age, rooms, 2)).to.equal(4320)
    expect(CalculateRent(age, rooms, 3)).to.equal(5280)
    expect(CalculateRent(age, rooms, 4)).to.equal(6240)
    expect(CalculateRent(age, rooms, 5)).to.equal(8160)

    done()
  })
})
