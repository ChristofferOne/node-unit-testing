const { describe, it } = require('mocha')
const { expect } = require('chai')
const request = require('request')

describe('Page status and content', function () {
  it('Main page content', function (done) {
    request('http://localhost:8080', function (error, response, body) {
      if (error) {}

      expect(body).to.equal('Hello World!')
      expect(response.statusCode).to.equal(200)

      done()
    })
  })

  it('Not found page', function (done) {
    request('http://localhost:8080/notanactualpage', function (error, response, body) {
      if (error) {}

      expect(response.statusCode).to.equal(404)

      done()
    })
  })
})
