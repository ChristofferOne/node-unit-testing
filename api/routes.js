const express = require('express')
const route = express.Router()

const { CalculateRent } = require('../calculateRent')

route.post('/calculate-rent', function (req, res) {
  const age = req.body.age
  const rooms = req.body.rooms
  const rating = req.body.rating

  const rent = CalculateRent(age, rooms, rating)

  res.setHeader('Content-Type', 'application/json')
  res.send(JSON.stringify(rent))
})

module.exports = route
