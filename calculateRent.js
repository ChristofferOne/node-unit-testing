const ratingMultipliers = {
  1: 0.7,
  2: 0.9,
  3: 1.1,
  4: 1.3,
  5: 1.7
}

const getRoomCost = (age) => {
  if (age < 25) {
    return 2600
  }

  if (age < 35) {
    return 2400
  }

  if (age > 65) {
    return 1800
  }

  return 2100
}

const CalculateRent = (age, rooms, rating) => {
  let roomCost = getRoomCost(age)
  roomCost = roomCost * rooms

  const rent = roomCost * ratingMultipliers[rating]

  return Math.floor(rent)
}

module.exports = { CalculateRent }
